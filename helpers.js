function getNewChannelId(channels, callback) {
    newId = 0;
    if (channels.length === 0) {
        newId = Math.floor(Math.random() * 1000);
    } else {
        while(true) {
            newId = Math.floor(Math.random() * 1000);
            hasDup = false;
            for (i = 0; i < channels.length; i++) {
                if (channels[i].id === newId) {
                    hasDup = true;
                    break;
                }
            }
            if (hasDup === false) {
                break;
            }
        }
    }
    
    return callback(newId);
}

function generateMap(callback) {
    // Generate the type and coordinate of the obstacle
    x = 25;
    obstacles = [];
    responses = [];
    while (x < 988) {
        y = Math.floor(Math.random() * 2) === 0 ? 1.5 : 3.7;
        obstacles.push({ x : x, y : y, type : Math.floor(Math.random() * 3) });
        x += 6 + Math.floor(Math.random() * 20);
    }
    
    // console.log(obstacles);
    
    // Create the response string
    obstacleData = '';
    for (i = 0; i < obstacles.length; i++) {
        if (i !== obstacles.length - 1) {
            obstacleData += obstacles[i].x + ':' + obstacles[i].y + ':' + obstacles[i].type + '|';
        } else {
            obstacleData += obstacles[i].x + ':' + obstacles[i].y + ':' + obstacles[i].type + ';';
        }
    }
    
    // Generate the initial position of the 2 motors
    pos = Math.floor(Math.random() * 2);
    
    res1 = 'data;' + obstacleData + pos + "\n";
    responses.push(res1);
    res2 = 'data;' + obstacleData + (1 - pos) + "\n";
    responses.push(res2);
    
    return callback(responses);
}

exports.getNewChannelId = getNewChannelId;
exports.generateMap = generateMap;
