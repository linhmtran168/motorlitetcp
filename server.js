var net = require('net'),
    carrier = require('carrier'),
    helpers = require('./helpers');

// Client class to hold player name and connection
function Client(connection) {
    this.name = null;
    this.onlineId = null;
    this.connection = connection;
    this.channel = null;
    this.reqTime = 0;
}

function Channel(id) {
    this.id = id;
    this.signCount = 0;
}

// Array to hold list of connected client
var clients = [];
// Array to hold list of channels
var channels = [];

//-----------TCP server part
// Define tcp server for the game
var tcpServer = net.createServer(function(conn) {
    console.log('Server connected');
    
    
    // Create a client object from the connection
    var client = new Client(conn);
    clients.push(client);
    
    conn.setTimeout(0);
    conn.setEncoding('utf8');
    
    // When the connection is estalish log to the console
    conn.on('connect', function() {
        console.log('Client connected');
    });
    
    // When a line arrive from the connection
    carrier.carry(conn, function(line) {
        console.log(line);
        lineArr = line.split(';');
        
        // If this is the first time client connect, set the username and id
        if (client.name === null) {
            client.name = lineArr[0];
            client.onlineId = lineArr[1];
            console.log('New client name: ' + client.name + "Id: " + client.onlineId);
            // Send response to this connected client
            conn.write('connected\n');
            // Send response to other connected clients
            clients.forEach(function(oneClient) {
                if (client.onlineId !== oneClient.onlineId) {
                    if (oneClient.connection !== null) {
                        oneClient.connection.write('new\n');
                    }
                }
            });
        }
        
        // A connect request from this connection to play with other client
        if (lineArr[0] === 'connect') {
            desClientId = lineArr[1];
            clients.forEach(function(oneClient) {
                if (oneClient.onlineId === desClientId) {
                    if (oneClient.channel !== null) {
                        client.connection.write('reply;' + desClientId +';busy\n');
                        console.log(oneClient.name + ' is busy');
                    } else {
                        oneClient.connection.write('connect;' + client.onlineId + '\n');
                        
                        // Create new channel and assign it to the players
                        helpers.getNewChannelId(channels, function(newId) {
                            channel = new Channel(newId);
                            client.channel = channel;
                            oneClient.channel = channel;
                            console.log("New channel created: " + channel.id);
                        });
                    }
                }
            });
        }
        
        // A response come from this connection to reply another request 
        if (lineArr[0] === 'reply') {
            desClientId = lineArr[1];
            clients.forEach(function(oneClient) {
                response = lineArr[2];
                
                if (oneClient.onlineId === desClientId) {
                    oneClient.connection.write('reply;' + client.onlineId + ';' + response + '\n');
                    
                    // Delete the channel if one player decline or cancel the request
                    if (response === 'decline' || response === 'cancel') {
                        channels.splice(channels.indexOf(client.channel),1);
                        console.log("Channel deleted: " + client.channel);
                        client.channel = null;
                        oneClient.channel = null;
                    }
                }
            });
        }
        
        // Test connection
        if (lineArr[0] === 'establish') {
            client.connection.write('ok 1\n');
            client.connection.write('ok 2\n');
        }
        
        // Generate the position of obstacles and the player in the game
        if (lineArr[0] === 'generate') {
            client.channel.signCount += 1;
            console.log("Number of request: " + client.channel.signCount);
            if (client.channel.signCount === 2) {
                console.log("Number of request: " + client.channel.signCount);
                
                // Generate map data (obstacle and initial players' position) and send them to the players
                helpers.generateMap(function(responses) {
                    // Send the data to the clients in the channel
                    clients.forEach(function(oneClient) {
                        if (oneClient.channel === client.channel) {
                            if (oneClient === client) {
                                oneClient.connection.write(responses[0]);
                                console.log("Data: " + responses[0]);
                            } else {
                                oneClient.connection.write(responses[1]);
                                console.log("Data: " + responses[1]);
                            }
                        }
                    });
                    
                    client.channel.signCount = 0;
                });
            }
        }
        
        
        // Handle play request/response
        if (lineArr[0] === 'play') {
            if (lineArr[1] === 'start') {
                if (client.reqTime < 1) {
                    client.channel.signCount += 1;
                    client.reqTime += 1;
                }

                console.log('Number of request: ' + client.channel.signCount);
                // Send the signal to start the game
                if (client.channel.signCount === 2) {
                    clients.forEach(function(oneClient) {
                        if (oneClient.channel === client.channel) {
                            console.log('Send start signal');
                            oneClient.connection.write('start\n');
                        }
                    });
                    client.channel.signCount = 0;
                }
            }
        }
        
        // Send and receive players state in game
        if (lineArr[0] === 'state') {
            clients.forEach(function(oneClient) {
                if (oneClient.channel === client.channel) {
                    if (oneClient !== client) {
                        oneClient.connection.write(line + '\n');
                        console.log('Write state: ' + line);
                    }
                }
            });
        }

        // If server receives a pause or resume signal, broadcast it to other players in the channel
        if (lineArr[0] === 'pause') {
            clients.forEach(function(oneClient) {
                if (oneClient.channel === client.channel) {
                    if (oneClient !== client) {
                        oneClient.connection.write(lineArr[0] + '\n');
                        oneClient.connection.write(lineArr[0] + '\n');
                        oneClient.connection.write(lineArr[0] + '\n');
                    }
                }
            });
        }

        if (lineArr[0] === 'resume') {
            clients.forEach(function(oneClient) {
                if (oneClient.channel === client.channel) {
                    if (oneClient !== client) {
                        oneClient.connection.write(lineArr[0] + '\n');
                        oneClient.connection.write(lineArr[0] + '\n');
                        oneClient.connection.write(lineArr[0] + '\n');
                        console.log('Send resume signal');
                    }
                }
            });
        }
    });
    
    // When the connection close
    conn.on('close', function() {
        // Get the position of this client in the clients array
        var pos = clients.indexOf(client);
        if (pos >= 0) {
            var username = client.name;
            var onlineId = client.onlineId;
            var toRemoveChann = client.channel;
            if (toRemoveChann !== null) {
                channels.splice(channels.indexOf(toRemoveChann), 1);
            }
            // Remove this client from the clients array
            clients.splice(pos, 1);
            console.log(username + ' disconnected');
            // Send notice to other clients
            clients.forEach(function(oneClient) {
                oneClient.connection.write(onlineId + ' disconnected\n');
                if (toRemoveChann !== null && oneClient.channel === toRemoveChann)  {
                    oneClient.channel = null;
                }
            });
        }
    });
});

tcpServer.listen(8088);

//--------- HTTP server part
var http = require('http'),
    journey = require('journey'),
    winston = require('winston');

// Create router function to return the list of all players
var createRouter = function() {
    var router = new (journey.Router)();
    
    router.path('/players', function() {
        this.get().bind(function(req, res) {
            // Get the name of all connected players
            var players = clients.map(function(client) {
                return { name: client.name, id: client.onlineId };
            });
            res.send(200, {}, { players: players });
        });
    });
    
    return router;
};

// HTTP server to return list of all players
var httpServ = http.createServer(function(req, res) {
    var body = '';
    var router = createRouter();
    
    winston.info('Incoming Request', { url: req.url });
        
    req.on('data', function(chunk) {
        body += chunk;
    });
        
    req.on('end', function() {
        // Dispatch the request to the service
        var emitter = router.handle(req, body, function(result) {
            res.writeHead(result.status, result.headers);
            res.end(result.body);
        });
        
        emitter.on('log', function(info) {
            winston.info('Request completed', info);
        });
    });
});

httpServ.listen(9099);
